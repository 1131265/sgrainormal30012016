#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#define _USE_MATH_DEFINES
#include <math.h>
#define DIST_PENDULOS 5
#define NUM_PENDULOS 2

using namespace std;

struct pendulo_t {
	float angulo;
	int direcao;
};

typedef struct Estado {
	GLint     delay;
	GLboolean doubleBuffer;
} Estado;

struct Pendulos {
	pendulo_t pendulos[NUM_PENDULOS];
};