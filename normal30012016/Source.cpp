﻿#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include "estruturas.h"
#define ALTURA_PENDULO 5
#define LARGURA_PENDULO 1.5
#define INC_ANGULO 1
#define MIN_ANGULO -45.0
#define MAX_ANGULO 45.0

Estado estado;
pendulo_t modelo;
Pendulos pendulos;

using namespace std;

void desenhaSegmento() {
	glBegin(GL_QUADS);
		glVertex3f(-(LARGURA_PENDULO/2.0), (ALTURA_PENDULO/2.0), 0.0);
		glVertex3f(-(LARGURA_PENDULO/2.0), -(ALTURA_PENDULO/2.0), 0.0);
		glVertex3f((LARGURA_PENDULO/2.0), -(ALTURA_PENDULO/2.0), 0.0);
		glVertex3f((LARGURA_PENDULO/2.0), (ALTURA_PENDULO/2.0), 0.0);
	glEnd();
}

// EXERCICIO A ALINEA I
void desenhaPendulo(float angulo) {
	glPushMatrix();

		glRotatef(angulo, 0.0, 0.0, 1.0);
		glTranslatef(0, -(ALTURA_PENDULO/2.0), 0);
		glColor3f(0.0, 0.0, 1.0);
		desenhaSegmento();
		cout << "y: " << ALTURA_PENDULO * sin(angulo * M_PI/180.0) << endl;
		glTranslatef(0.0, -ALTURA_PENDULO/2.0f, 0.0);
		glRotatef(-angulo, 0.0, 0.0, 1.0);
		glTranslatef(0.0, -ALTURA_PENDULO/2.0f, 0.0);
		glColor3f(0.0, 1.0, 0.0);
		desenhaSegmento();

	glPopMatrix();
}

void desenhaDoisPendulos() {
	glPushMatrix();
	for (int i = 0; i < NUM_PENDULOS; i++) {
		desenhaPendulo(pendulos.pendulos[i].angulo);
		glTranslatef(DIST_PENDULOS, 0.0, 0.0);
	}
	glPopMatrix();
}

void init() {
	// cor do fundo
	glClearColor(0.63, 0.21, 0.66, 0.0);

	// inicializar o valores de visualização
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-1, 1, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void initEstado() {
	estado.delay = 10;
	estado.doubleBuffer = 1;
}

void initPendulo() {
	modelo.angulo = 0;
	modelo.direcao = -1;
	pendulos.pendulos[0].angulo = -45.0;
	pendulos.pendulos[0].direcao = -1.0;
	pendulos.pendulos[1].angulo = -45.0;
	pendulos.pendulos[1].direcao = -1.0;
}

void desenhaReferencialCartesiano()
{
	glColor3f(1.0, 1.0, 1.0);

	//x
	glBegin(GL_LINES);
	glVertex3f(0.0, -1.0, 0.0);
	glVertex3f(0.0, 1.0, 0.0);
	glEnd();

	//y
	glBegin(GL_LINES);
	glVertex3f(-1.0, 0.0, 0.0);
	glVertex3f(1.0, 0.0, 0.0);
	glEnd();
}

void desenhaCena() {
	glClear(GL_COLOR_BUFFER_BIT);

	glPushMatrix(); 
	desenhaReferencialCartesiano();
	glPopMatrix();

	glPushMatrix();
	glScalef(0.1, 0.1, 0.1);
	//desenhaPendulo(modelo.angulo);
	// alinea III do exercicio b
	desenhaDoisPendulos();
	glPopMatrix();
	glFlush();

	if (estado.doubleBuffer) {
		glutSwapBuffers();
	}
}

void Timer(int value) {
	glutTimerFunc(estado.delay, Timer, 0);

	modelo.angulo += INC_ANGULO * modelo.direcao;

	if (modelo.angulo > MAX_ANGULO || modelo.angulo < MIN_ANGULO) {
		modelo.direcao *= -1;
	}

	glutPostRedisplay();
}

// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{
	GLint size;

	if (width < height)
		size = width;
	else
		size = height;

	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint)size, (GLint)size);


	// Matriz Projec��o
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// gluOrtho2D(left,right,bottom,top); 
	// projec��o ortogonal 2D, com profundidade (Z) entre -1 e 1
	gluOrtho2D(-1, 1, -1, 1);

	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformac�es dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB);

	glutInitWindowSize(500, 500);

	glutInitWindowPosition(100, 100);

	glutCreateWindow("pendulo");

	init();
	initEstado();
	initPendulo();

	glutTimerFunc(estado.delay, Timer, 0);

	glutReshapeFunc(Reshape);
	glutDisplayFunc(desenhaCena);

	glutMainLoop();

	return 0;
}